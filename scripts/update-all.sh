#!/usr/bin/env bash
set -euo pipefail

# change into content directory
cd ../content/

# loop over stuff
for i in $(find . -name "*.md" -type f); do
    # ignore anything with tosort in its path
    if [[ ! "$i" == *"tosort"* ]]; then
        # execute the html create script for all the others
        bash ../scripts/create.sh "$i"
    fi
done
