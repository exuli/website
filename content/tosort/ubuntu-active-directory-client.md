# Active Directory in Ubuntu 20.10

Last Edited: 04.12.2020  

## After installation

Execute the command as root  
`realm join -U (user) (domain)`  

To log in as an AD-user, enter (domain name)\(username) on the login screen.  

## Give AD User sudo

