# DVD Ripping Speed

Last Edited: 02.05.2021  

## Methodology
The DVD was opened in Celluloid to allow tools like `dd` or `cat` to make a bit-copy of the dvd  
The target device was a RAID 1 of HDDs with BTRFS as the file system  

`time (command)`  
Using fish shell  
`sudo` had been executed beforehand to eliminate variance in entering the sudo password  

After every test, the DVD was ejected and remounted and the created iso has been deleted  

## Tests

### DD 1M block size

#### Matrix
`time dd if=/dev/sr0 of=matrix.iso bs=1M`  
32.25 mins  

### Cat

#### Matrix
`time cat /dev/sr0 > matrix.iso`  
32.26 mins  

#### Inglourious Basterds
`time cat /dev/sr0 > inglourious_basterds.iso`  
31.19 mins  

#### 300
`time cat /dev/sr0 > 300.iso`  
26.44 mins  
