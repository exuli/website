# Additional IP addresses

Last Edited: 16.03.2021  

## Temporary
`# ip address add (address)/(cidr) dev (device)`  

## Permanent with config
Edit `/etc/network/interfaces`  

*Example: (Potentially causes issues)*  
```
auto enp3s0
allow-hotplug enp3s0

# primary settings ipv4
iface enp3s0 inet static
  address 144.76.31.55
  netmask 255.255.255.224
  gateway 144.76.31.33

iface enp3s0 inet static
  address 144.76.31.26
  netmask 255.255.255.224

# primary settings ipv6
iface enp3s0 inet6 static
  address 2a01:4f8:191:2236::2
  netmask 64
  gateway fe80::1

iface enp3s0 inet6 static
  address 2a01:4f8:191:2236::10
  netmask 64

iface enp3s0 inet6 static
  address 2a01:4f8:191:2236::20
  netmask 64

iface enp3s0 inet6 static
  address 2a01:4f8:191:2236::30
  netmask 64

iface enp3s0 inet6 static
  address 2a01:4f8:191:2236::40
  netmask 64
```

*Example 2:*  
```
auto enp3s0
allow-hotplug enp3s0

# primary ipv4
iface enp3s0 inet static
  address 144.76.31.55
  netmask 255.255.255.224
  gateway 144.76.31.33

up ip addr add 144.76.31.26 dev enp3s0
down ip addr del 144.76.31.26 dev enp3s0

# primary ipv6
iface enp3s0 inet6 static
  address 2a01:4f8:191:2236::2
  netmask 64
  gateway fe80::1

up ip addr add 2a01:4f8:191:2236::10/64 dev enp3s0
down ip addr del 2a01:4f8:191:2236::10/64 dev enp3s0

up ip addr add 2a01:4f8:191:2236::20/64 dev enp3s0
down ip addr del 2a01:4f8:191:2236::20/64 dev enp3s0

up ip addr add 2a01:4f8:191:2236::30/64 dev enp3s0
down ip addr del 2a01:4f8:191:2236::30/64 dev enp3s0

up ip addr add 2a01:4f8:191:2236::40/64 dev enp3s0
down ip addr del 2a01:4f8:191:2236::40/64 dev enp3s0

```

## References
[See the docs by hetzner](https://docs.hetzner.com/robot/dedicated-server/network/net-config-debian)  
