# Network Layout for testing

Last Edited: 19.04.2021  

## Networks

### Public
- vpnpub
- Bridged interface

### Network 1
- vpnnet1
- OPNSense router
- 192.168.7.1/24
- DHCP-assigned Host (192.168.7.30)

### Network 2
- vpnnet2
- OPNSense router
- 192.168.8.1/24
- DHCP-assigned Host (192.168.8.30)

### Diagram
![](./network-layout/network-layout.png)
