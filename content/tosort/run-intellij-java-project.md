# Run Intellij Java Project in Terminal

Last Edited: 25.03.2021  

## Command Example
Run compiled java project in `/home/marc/Dokumente/BBW/Modul_1\ 404/Intellij/auto_object/out/production/auto_object`  
Main.class is the central file  
`$ java -classpath /home/marc/Dokumente/BBW/Modul_1\ 404/Intellij/auto_object/out/production/auto_object Main`  
With package path  
`$ java -classpath /home/marc/Dokumente/BBW/Modul_1\ 404/Intellij/ticket-simple/src/main/java org.example.App`  

`$ java Main` might also work  

## Compile project
Compiling the main class will also compile all the classes it depends on  
The compiled .class files will be in the same folder  
`$ javac Main.java Object.java`  

`$ javac Main.java` should suffice  
