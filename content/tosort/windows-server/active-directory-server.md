# Active Directory

Last Edited: 22.01.2021  

## Server with Desktop

### Install roles

Configuration in Server Manager.  

1. Add new role to the server
2. role/feature-based installation
3. Select "Active Directory Domain Services"

### Setup Active Directory

After the installation there should now be a yellow triangle next to the notifications icon in Server Manager.  
In this notification the Server has to be promoted to a domain controller.  

Select "Add a new Forest" to create a new domain.  
The root domain should be a real webdomain that is owned by you. Add a subdomain to it to prevent any conflicts with the actual domain.  

- Set a password for the Domain Controller
- Don't change anything under DNS Options

In "Additional Options" the name of the domain can be changed. **This name can consist of 15 characters max**  

Next up it's possible to change the database location.  

Now the whole configuration can be confirmed. Finally the configuration will be tested. If the test passed, it's possible to install this configuration now.  

### Second Domain Controller

In the notifications there should be a yellow triangle prompting to promote this server to a domain controller.  

Select "Add a Domain Controller to an existing domain", supply the proper credentials and select the domain you want to join.  
The credentials have to be for an admin on that domain.  

Enter the proper DSRM password  

Don't change anything under DNS Options and Additional Options.  

You can change the database locations.  

Review your options and start the check once you're satisfied.  

#### SID of Domain identical to machine

Press "Win + R" and enter "sysprep"  

Execute the sysprep.exe  

Choose "OOBE" and select "Generalize"  

The Server will reboot and ask some questions  

**Make sure to fix the name of the server again!**  

### DFS File Server with Replication

In Server Manager go to `File and Storage Services > Volumes > Disks`  

Create a new volume on the Disk you want to use.  

Make sure to use NTFS. Other than that, use whatever settings you like  

Add a two new roles to each server.
`File and Storage Services > File and iSCSI Services > DFS Namespaces`  
`File and Storage Services > File and iSCSI Services > DFS Replication`  

Under "Tools" go to "DFS Management"  

In "Namespaces" create a new Namespace  

You will have to enter your first server there  

On the next page you have to enter a name for the share. Make sure to edit the settings and select the path you want to share.  

Now you can select the address of your share and finish the setup.  

After you finished the setup change into your Namespace and go to "Namespace Servers".  

Add a new namespace server and choose your second file server. Make sure to use the same permissions as with the last server and select the path you want.  

After adding all servers to a namespace, go to "Replication" and create a new replication group.  

Leave "Multipurpose replication group" selected  

The name of the replication group is only for your convenience  

Now you have to add your servers  

With few servers "Full mesh" is fine  

Here you can set bandwidth limits based on the current time or all-day bandwidth limits  

You have to choose the first server now and select the proper path  

Next you have to edit every other member to select the proper path on those servers as well  

Finally you can look over the settings again and start the creation  

## Server Core

### Join Domain

`sconfig`  

Select option 1  

Choose "D" for Domain and enter the address for the domain.  

Log in as a user on that domain. (domain)\(user)  

To join the domain, the pc has to be rebooted.  

Make sure to log in as your domain admin after the reboot  

### Second Domain Controller

`powershell`  

Install domain services  
`install-windowsfeature ad-domain-services`  

`install-addsdomaincontroller -installdns -credential (get-credential (user)) -domainname (domain address)`  

## Check Domain Controller

### Powershell

If this returns 2, the server is acting as a domain controller.  
`(get-ciminstance -classname win32_operatingsystem).producttype`  

## Remove Domain Controller

Go to `Active Directory Users and Computers`  

Under `Domain > Domain Controllers` delete the Server you want to remove.  

Make sure to select `Delete this Domain Controller anyway. ...`  

Open the Tool `Active Directory Sites and Services`  

Under `Sites > Default-First-Site-Name > Servers` delete the Server you want to remove.  

Open the terminal  

`ntdsutil`  

`metadata cleanup`  

`remove selected server (server)`  
