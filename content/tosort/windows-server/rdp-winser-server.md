# RDP on Windows

Last Edited: 04.01.2021  

## Configuration

Go to `Local Server` in the Server manager.  
There should be an option called `Remote Desktop`. Click on it and allow remote connections.  
If you refresh the view now, `Remote Desktop` should show as enabled.  

### Allow unlimited RDP sessions

Enter `gpedit` in the search bar  

Go to `Administrative Templates>Windows Components>Remote Desktop Services>Remote Desktop Session Host>Connections`  

Disable `Limit number of connections`  

Disable `Restrict Remote Desktop Services users to a single Remote Desktop Services session`  

Reboot the Server  

## References

[Linux: RDP client configuration](/content/linux-desktop/rdp-linux-client.html)  
[Windows Pro: RDP client configuration](/content/windows-desktop/rdp-winpro-client.html)  
[Linux: RDP server configuration](/content/linux-server/debian/rdp-linux-server.html)  
[Windows Pro: RDP server configuration](/content/windows-desktop/rdp-winpro-server.html)  
