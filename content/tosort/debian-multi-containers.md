# All Services on one machine

Last Edited: 09.02.2021  

## Packages
Currently only available in Debian 11 (Bullseye) and newer  
`# apt install podman`  

`# apt install nginx certbot python3-certbot-nginx`  

## Website
The website will run on nginx  

Sample configuration with ssl stuff included  
```
server {
    root /var/www/website; #change this for different path to website directory
    index index.html; #name of main file
    server_name DOMAIN_NAME; #this is what certbot will make certificates for
    location / {
       try_files $uri $uri/ =404;
     }

    listen [2a01:4f8:191:2236::10]:443 ssl; #set ipv6 address
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;



    add_header Strict-Transport-Security "max-age=31536000" always;


    ssl_trusted_certificate /etc/letsencrypt/live/DOMAIN_NAME/chain.pem;
    ssl_stapling on;
    ssl_stapling_verify on;



}

server {
    if ($host = DOMAIN_NAME) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen [2a01:4f8:191:2236::10]:80; #ipv6, to specify an address use [(address)]:(port)
    root /var/www/website; #change this for different path to website directory
    index index.html; #name of main file
    server_name DOMAIN_NAME; #this is what certbot will make certificates for
    location / {
       try_files $uri $uri/ =404;
     }
}
```

Enable the config  
`$ ln -s /etc/nginx/sites-available/(config) /etc/nginx/sites-enabled/`  

Restart nginx  
`# systemctl restart nginx`  

## Nextcloud
Pulls latest stable release  
`$ podman pull docker://nextcloud`  

TODO
Nextcloud will be using apache  
`# apt install mlocate apache2 libapache2-mod-php mariadb-client mariadb-server wget unzip bzip2 curl php php-common php-curl php-gd php-mbstring php-mysql php-xml php-zip php-intl php-apcu php-redis php-http-request python-certbot-apache php-bcmath php-gmp php-imagick`  

No password set  
`# mariadb -u root -p`  

`CREATE DATABASE nextcloud;`  

`GRANT ALL ON nextcloud.* TO 'nextcloud'@'localhost' IDENTIFIED BY '<password>';`  

`FLUSH PRIVILEGES;`  

Exit the MariaDB prompt  

Download Nextcloud into `/var/www`  
`$ wget https://download.nextcloud.com/server/releases/nextcloud-(version).tar.bz2`  

`$ tar -xf nextcloud-(version)`  

Change owner to nextcloud user  
`# chown -Rfv www-data:www-data /var/www/nextcloud`  

Create nextcloud configuration for apache  
`# vi /etc/apache2/sites-available/nextcloud.conf`  

Configuration file  
```
<VirtualHost *:80> #specify listen ip addresses: (address):(port) for ipv4, [(address)]:(port) vor ipv6, *:80 for all
ServerAdmin webmaster@localhost
DocumentRoot /var/www/nextcloud
Alias /nextcloud "/var/www/nextcloud/"
    
<Directory "/var/www/nextcloud/">
Options +FollowSymlinks
AllowOverride All

<IfModule mod_dav.c>
Dav off
</IfModule>

Require all granted

SetEnv HOME /var/www/nextcloud
SetEnv HTTP_HOME /var/www/nextcloud
</Directory>

ErrorLog ${APACHE_LOG_DIR}/nextcloud_error_log
CustomLog ${APACHE_LOG_DIR}/nextcloud_access_log common
</VirtualHost>
```

Enable nextcloud and disable the default site  
`# a2ensite nextcloud.conf && a2dissite 000-default.conf`  

Edit `ports.conf` for apache2 to only bind the addresses you need  

`# systemctl restart apache2`  

## Jellyfin
Pulls latest stable release  
`$ podman pull docker://jellyfin/jellyfin`  

Create persistent storage  
`$ podman volume create jellyfin-config`  
`$ podman volume create jellyfin-cache`  

Start Jellyfin  
```
podman run -d \
    --name jellyfin \
    --cgroup-manager=systemd \
    --volume jellyfin-config:/config \
    --volume jellyfin-cache:/cache \
    --volume (media path):/media \
    --net=host \
    jellyfin/jellyfin
```

Create a new nginx configuration file in `/etc/nginx/sites-available`  
*Make sure to replace "DOMAIN\_NAME" with your domain name*  
```
server {
    server_name DOMAIN_NAME;

    # use a variable to store the upstream proxy
    # in this example we are using a hostname which is resolved via DNS
    # (if you aren't using DNS remove the resolver line and change the variable to point to an IP address e.g `set $jellyfin 127.0.0.1`)
    set $jellyfin 127.0.0.1;
    #resolver 127.0.0.1 valid=30;

    # Security / XSS Mitigation Headers
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    location = / {
        return 302 https://$host/web/;
    }

    location / {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;

        # Disable buffering when the nginx proxy gets very resource heavy upon streaming
        proxy_buffering off;
    }

    # location block for /web - This is purely for aesthetics so /web/#!/ works instead of having to go to /web/index.html/#!/
    location = /web/ {
        # Proxy main Jellyfin traffic
        proxy_pass http://$jellyfin:8096/web/index.html;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    location /socket {
        # Proxy Jellyfin Websockets traffic
        proxy_pass http://$jellyfin:8096;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Protocol $scheme;
        proxy_set_header X-Forwarded-Host $http_host;
    }

    listen [2a01:4f8:191:2236::2]:443 ssl; #set ipv6 address
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
}

server {
    if ($host = DOMAIN_NAME) {
        return 301 https://$host$request_uri;
    }

    listen [2a01:4f8:191:2236::2]:80; #set ipv6 address
    server_name DOMAIN_NAME;
    return 404;

}
```

## Matrix
TODO
`# apt install build-essential python3-dev libffi-dev python3-pip python3-setuptools sqlite3 libssl-dev python3-virtualenv libjpeg-dev libxslt1-dev certbot python-certbot-nginx nginx`  

Preparing virtual environment  
`# mkdir -p /etc/synapse`  
`# virtualenv -p python3 /etc/synapse/env`  
`# source /etc/synapse/env/bin/activate`  

Install python packages  
`# pip3 install --upgrade pip virtualenv six packaging appdirs setuptools matrix-synapse`  

`$ cd /etc/synapse`  

Set synapse configuration  
*Make sure to replace "DOMAIN\_NAME" with your domain name*  
```
python -m synapse.app.homeserver \
  --server-name DOMAIN_NAME \
  --config-path homeserver.yaml \
  --generate-config \
  --report-stats=no
```

Edit `/etc/synapse/homeserver.yaml`  
Search for `bind_addresses:` and set it to `['127.0.0.1']`  

Create an nginx config file `/etc/nginx/sites-available/matrix`
*Make sure to replace "DOMAIN\_NAME" with your domain name*  
```
server {
    listen 80; #ipv4 address
	listen [::]:80; #ipv6 address
    server_name DOMAIN_NAME;
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl; #ipv4 address
    listen [::]:443 ssl; #ipv6 address
    server_name DOMAIN_NAME;

    ssl on;
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;

    location / {
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}

server {
    listen 8448 ssl default_server; #ipv4 address
    listen [::]:8448 ssl default_server; #ipv6 address
    server_name DOMAIN_NAME;

    ssl on;
    ssl_certificate /etc/letsencrypt/live/DOMAIN_NAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/DOMAIN_NAME/privkey.pem;
    location / {
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
    }
}
```

`$ ln -s /etc/nginx/sites-available/(config) /etc/nginx/sites-enabled/`  

Restart nginx  

`# cd /etc/synapse`  
`# source /etc/synapse/env/bin/activate`  
`# synctl start`  

Create new user through interactive dialogue  
`# register_new_matrix_user -c homeserver.yaml http://localhost:8008`  

## HTTPS
For nginx  
`# certbot --nginx --agree-tos --redirect --hsts --staple-ocsp --email (email) -d (domain1),(domain2)`  
Use certonly with Website, Jellyfin and Matrix  
`# certbot certonly ...`  

For apache  
`# certbot --apache --agree-tos --redirect --hsts --staple-ocsp --email (email) -d (domain1),(domain2)`  

Enable cron timer for renew  
`$ crontab -e`  

```
1 1 1 * * certbot renew
```
