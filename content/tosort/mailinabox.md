# Mail in a Box

Last Edited: 15.04.2021  

## DNS Preparation
First something called "Glue Records" has to be set up.  
In Porkbun the screen should look like the following picture.  
Note that both the NS1 and NS2 records point to our mailserver  
![](./mailinabox/1_glue_records_dns.png)  
Next, our server has to be entered as nameserver  
![](./mailinabox/2_change_ns.png)  
Here are the normal records set for the mailserver  
![](./mailinabox/3_records.png)  

## Installation
**Only Ubuntu 18.04 is currently supported**  
`$ curl -s https://mailinabox.email/setup.sh | sudo -E bash`  
You will be guided throug the installation  
