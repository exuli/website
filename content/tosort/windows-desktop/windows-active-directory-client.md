# Active Directory Client

Last Edited: 04.12.2020  

## DNS Server

Set the domain server as DNS server.  

### Method 1 (Recommended)

Set the DNS server in the DHCP server. This way any device will automatically detect it.  

### Method 2

Manually set the DNS server in the networkadapter settings.  

Whether the DNS works can be checked by using `nslookup (domain name)`  

## Join Domain

Go to "Access work or school" under Accounts in the windows settings.  

Click "Connect" and select "Join this device to a local Active Directory Domain" at the bottom.  

Enter the Domain and log in with a domain user.  

## Install Server Manager

Go to the settings for "Apps & Features"  

Select "RSAT: Server Manager" and any additional components you might need.  
