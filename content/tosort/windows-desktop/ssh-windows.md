# SSH on Windows

Last Edited: 18.01.2021  

## Client

Open PowerShell as administrator  
`Add-WindowsCapability -Online -Name OpenSSH.Client`  

## Server

Open PowerShell as administrator  
`Add-WindowsCapability -Online -Name OpenSSH.Server`

Start service  
`Start-Service sshd`  

Enable service  
`Set-Service -Name sshd -StartupType 'Automatic'`  

Check whether firewall rule exists  
`Get-NetFirewallRule -Name *ssh*`  

Create firewall rule for port 22  
`New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22`  

## References

[OpenSSH Installation](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)  
