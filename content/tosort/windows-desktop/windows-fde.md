# Full Disk encryption on Windows

Last Edited: 29.01.2021  

## Bitlocker
**The keyboard layout when Bitlocker is asking for your password might not be the correct one**  

If you do not have a TPM, follow the steps under [Group Policy](#group-policy) to enable Bitlocker regardless.  

### Group Policy
Open "gpedit"  

Change `Computer Configuration > Administrative Templates > Windows Components > Bitlocker Drive Encryption > Operating System Drives > Require additional authentication at startup` to "Enabled"  

After setting the group policy you can right click on the C: drive and choose Bitlocker.  

You can choose either a USB drive or a password to unlock your machine.  

After setting a password you have to choose where you want the recovery key to be stored.  

It is recommended to encrypt the whole drive  

## Veracrypt
**The keyboard layout used it US**  

Veracrypt has to be installed first  

After the installation open Veracrypt, select "Create Volume" and choose "Encrypt System partition".  

Choose whether the system is single-boot or multi-boot.  

You can also change the encryption algorithm used.  

After setting the password have some fun moving your mouse around.  

One of the last steps is selecting a location for the rescue drive.  

The encryption has to be tested first. Click the "Test" button for that.  

After the reboot, open Veracrypt again and select "Encrypt"  
