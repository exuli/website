# RDP on Windows 10 Pro

Last Edited: 05.01.2021  

## Configuration

Go to `Remotedesktop` in the settings under `System`  

### Change port

*PowerShell with admin*  

Check port in use currently:  
`Get-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "PortNumber"`  

Change port:  
`Set-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Control\Terminal Server\WinStations\RDP-Tcp' -name "PortNumber" -Value (port)`  

Firewall exception:  
`New-NetFirewallRule -DisplayName 'RDPPORTLatest' -Profile 'Public' -Direction Inbound -Action Allow -Protocol TCP -LocalPort (port)`  

Reboot the PC  

## References

[Linux: RDP client configuration](/content/linux-desktop/rdp-linux-client.html)  
[Windows Pro: RDP client configuration](/content/windows-desktop/rdp-winpro-client.html)  
[Linux: RDP server configuration](/content/linux-server/debian/rdp-linux-server.html)  
[Windows Server: RDP server configuration](/content/windows-server/rdp-winser-server.html)  
