# Git SSH keys

Last Edited: 04.01.2021  

## Creating Key

[Info on ssh keys](/content/linux-all/ssh.html)  

Change into the BASH shell and enter the next two commands in there  
```bash
eval "$(ssh-agent -s)"
```

```bash
ssh-add (path to private key file)
```

## Add key to gitlab/github

Add the public key to the github/gitlab profile.  

Set the ssh keyfile in $HOME/.ssh/config. Make sure to edit with sudo privileges.  

For github:  
```
Host github.com
      IdentityFile (path to private key file)
```

For gitlab:  
```
Host gitlab.com
      IdentityFile (path to private key file)
```

Make sure to clone all projects through the ssh address instead of https.  
```bash
git clone (repository ssh address)
```


