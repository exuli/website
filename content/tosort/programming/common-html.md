# Common HTML

Last Edited: 09.12.2020  

## Basic template

```html
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8" />
        <title>(title shown in Tab)</title>
        <!-- Externes Stylesheet --> 
        <link rel="stylesheet" href="(path to css file)">
        <!-- Internes Styling --> 
        <!--
        <style>
            body {
                color: #eeeeee;
            }
        </style>
        --> 
        <link rel="icon" href="(path to icon (png, ico))">
    </head>
    
    <body>
        <h1>(Main Title)</h1>
        <p>(Text)</p>
        
        <h2>(Minor Title)</h2>
        <!-- Comment -->
    </body>
</html>
```

## Media

### Images

```html
<img src="(source)" alt="(alternative text)" width="" height="" />
```
Make sure that width and height are the actual image resolutions. **Do not use them to scale images!**  
Also make sure to have the proper aspect ratio.  

### Videos

```html
<video width="" height="" controls>
    <source src="(source)" type="video/(type)">
</video>
```
Width and height should again not be used to scale the video.  
If an improper aspect ratio is set, the smaller value will be used for the actual video which will maintain aspect ratio, while the controls get placed where they should be according to this height/width.  
Controls enables video controls.  
Video type has to match the source.  

## Lists

### ordered

```html
<ol>
    <li></li>
    <li></li>
</ol>
```

### unordered

```html
<ul>
    <li></li>
    <li></li>
</ul>
```
