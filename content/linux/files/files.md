# File operations

Last Edited: 08.12.2020  

## Workings of file permissions

### Change permissions

To change file permissions use `chmod (-R) XXX (path)`  

XXX signify the permissions for the file's owner/group/others respectively  

Each X goes from 0 to 7.  
What each number means can be easily calculated by looking at what the individual bit values mean.  
```
0 -> No Permission
4 -> Read Permission
2 -> Write Permission
1 -> Execute Permission
```

A value of 5 therefor gives the permissions "Read" and "Execute".  

*To enter a folder, you need the read as well as the execute permission!*  

### Change user and group

Use `chown` to change the owner and group of a file or directory.  

Example:  
`chown (-R) (owner):(group) (path)`  

### Change group

Use `chgrp` to change the group of a file or directory.  

Example:  
`chgrp (-R) (group) (path)`  

## Find biggest files

`find . -type f -print0 | xargs -0 du -s | sort -n | tail -(amount) | cut -f2 | xargs -I{} du -sh {}`  

`find . -type f -printf "%s %p\n" | sort -nr | head -5`  
