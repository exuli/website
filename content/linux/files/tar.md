# Tar

Last Edited: 17.01.2021  

## Create Archive

### with zstd

Use all cores with zstd  
`tar -cv -I"zstd -T0" -f (archive).tar.zst (source)`  

#### Set compression levels

Use "-XX" to set compression levels. 1, the least compression. 19, the most (normal) compression.
`tar -cv -I"zstd -19 -T0" -f (archive).tar.zst (source)`  

## Extract Archive


