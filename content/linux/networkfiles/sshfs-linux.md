# SSHFS on Linux

Last Edited: 18.01.2021  

## Usage

`# apt install sshfs`  

`# pacman -S sshfs`  

Mount remote filesystem  
`sshfs (user)@(ip/domain):(remotepath) (mountpoint)`  

*Example with Windows host:*  
`sshfs admin@192.168.1.123:/ /mnt/tmp`  

## References

[sshfs](https://github.com/libfuse/sshfs)  
