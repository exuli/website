# telnet

Last Edited: 04.01.2021  

## Change telnet port

Edit `/etc/services`  
In the line starting with "telnet", change the port number.  

Restart the appropriate service. (Either telnetd, xinetd or inetd)  

## Connect with telnet

`telnet (ip) (optional: port)`  
