# RDP client on Linux
## Installation
Use Remmina as client and install freerdp to get support for RDP.  

`# pacman -S remmina freerdp`  

## Configuration
Example configuration:  
![](./rdp-linux-client/rdp-linux-client-pic1-example.png)  

### Set different port
![](./rdp-linux-client/rdp-linux-client-pic2-port.png)  

## References
[Windows Pro: RDP client configuration](/content/windows-desktop/rdp-winpro-client.html)  
[Linux: RDP server configuration](/content/linux-server/debian/rdp-linux-server.html)  
[Windows Server: RDP server configuration](/content/windows-server/rdp-winser-server.html)  
[Windows Pro: RDP server configuration](/content/windows-desktop/rdp-winpro-server.html)  

### External
[ArchWiki Remmina](https://wiki.archlinux.org/index.php/Remmina)  
