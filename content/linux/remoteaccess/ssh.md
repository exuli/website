# SSH

Last Edited: 04.01.2021  

## Client

### Configuration file

`/etc/ssh/ssh_config`  

### Connect to non-standard port

`$ ssh -p (port) (user)@(ip)`  

### X11 passthrough

`$ ssh -X (user)@(ip)`  

### ssh keys

Create new key:  
`$ ssh-keygen`  

*Example* for ed25519 key:  
`$ ssh-keygen -t ed25519`  

The "-C" flag can be used to add comments in ssh key files.  

Enable the ssh key:  
`$ ssh-copy-id -i (public key file) (user)@(ip/domain)`  

If you are copying the ssh key from a different client, use the "-f" flag  
`$ ssh-copy-id -f -i (public key file) (user)@(ip/domain)`  

## Server

### Installation

#### Debian

`# apt install openssh-server`  

#### Arch

`# pacman -S openssh`  
<br>
`# systemctl enable ssh`  

### Configuration file

`/etc/ssh/sshd_config`  

Make sure to restart the sshd service after changes.  

### Change port

Uncomment `Port` and set any port number  

### Root login

`PermitRootLogin` setting  

```
yes -> Able to log in with password as root
```

### Password Authentication

`PasswordAuthentication` setting  

```
yes -> Allow login with passwords
no -> Only allow ssh keys
```
