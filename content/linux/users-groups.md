# Users and Groups

Last Edited: 04.01.2021  

## Users

Check users by looking at `/etc/passwd`  

### Add users

Basic usage:  
`# useradd -m (user)`  

Important options:  
```
login name -> by default
group -> -G //separate multiple by commas: group1,group2
with home dir -> -m
shell -> -s
full name -> -c
```

Example more complicated usage:  
`# useradd -m -c "Bruno Huber" -s /bin/bash -G buchhaltung,sudo bruhub`  

### Add user to groups

Add user to more groups:  
`# usermod -a -G (group1),(group2) (user)`  

Alternative command:  
`# gpasswd -a (user) (group)`  

### Remove user from group

`# gpasswd -d (user) (group)`  

## Groups

Check a user's groups with `id (user)`  

### Create group

`# groupadd (group)`  

### Rename group

`# groupmod -n (new_group) (old_group)`  

### Delete group

`# groupdel (group)`  
