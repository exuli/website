# PXE boot on Debian

Last Edited: 20.01.2021  

## Prerequisites
Method 1 (described below):  
The PXE Server is also acting as the DHCP Server  

Method 2:  
TFTP is passed along from the router to the PXE Server  

## Files
`# apt install dnsmasq pxelinux syslinux-efi syslinux nfs-kernel-server`  

`# mkdir -p /netboot/{bios,uefi}`  

BIOS files  
`# cp /usr/lib/PXELINUX/pxelinux.0 /netboot/bios`  
`# cp /usr/lib/syslinux/modules/bios/{ldlinux.c32,libcom32.c32,libutil.c32,vesamenu.c32} /netboot/bios`  

For Windows also copy this file  
`# cp /usr/lib/syslinux/memdisk /netboot/bios`  

*Not working yet*  
UEFI files  
`# cp /usr/lib/syslinux/modules/efi64/ldlinux.e64 /netboot/uefi`  
`# cp /usr/lib/syslinux/modules/efi64/{vesamenu,libcom32,libutil,menu}.c32 /netboot/uefi`  
`# cp /usr/lib/SYSLINUX.EFI/efi64/syslinux.efi /netboot/uefi`  

Boot directory  
`# mkdir /netboot/boot`  

## OS

### Debian 10
*Not working*  
Make directory for distros  
`# mkdir -p /netboot/boot/amd64/debian/10`  

Mount the iso  
`# mount -o loop -t iso9660 "$HOME/Downloads/debian-10.7.0-amd64-netinst.iso /media`  

Copy files to our distro directory  
(Might have to install rsync)  
`# rsync -av /media/ /netboot/boot/amd64/debian/10`  

`# umount /media`  

#### Netboot image
[netboot image](http://ftp.nl.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/gtk/)  

`$ wget http://ftp.nl.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/gtk/netboot.tar.gz`  

`# cp netboot.tar.gz /netboot/boot/amd64/debian/10-net`  

Unpack everything  
`# tar -xvf /netboot/boot/amd64/debian/10-net/netboot.tar.gz`  

### Ubuntu 20.04
`$ wget "https://releases.ubuntu.com/20.04.1/ubuntu-20.04.1-desktop-amd64.iso"`  

`# mkdir -p /netboot/boot/amd64/ubuntu/20-04`  

`# mount -o loop -t iso9660 ubuntu-20.04.1-desktop-amd64.iso /media`  

`# rsync -av /media/ /netboot/boot/amd64/ubuntu/20-04`  

`# umount /media`  

### Windows 10
*EVERYTHING IS BROKEN. DON'T EVER ATTEMPT.*  
Download the [Assessment Toolkit and Preinstallation Environment](https://docs.microsoft.com/en-us/windows-hardware/get-started/adk-install)  
Install the ADK, including the PE addon  

Run "Deployment and Imaging Tools Environment" as administrator  

#### guide 1
[Windows network installation](https://www.tekfik.com/kb/linux/windows-network-installation-through-linux-pxe-server)  
`copype amd64 C:\WinPE_amd64`  

`Dism /Mount-Image /ImageFile:"C:\WinPE_amd64\media\sources\boot.wim" /index:1 /MountDir:"C:\WinPE_amd64\mount"`  

`notepad c:\WinPE_amd64\mount\Windows\System32\Startnet.cmd`  
```
wpeinit
net use Z: \(pxe server ip)\(smb share name)\(folder name)/user:(username) (password)
Z:\setup.exe
```

`Dism /Unmount-Image /MountDir:"c:\WinPE_amd64\mount" /commit`  
`MakeWinPEMedia /ISO c:\WinPE_amd64 c:\win10pe.iso`  

#### guide 2
[Bootable win pe](https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/winpe-create-usb-bootable-drive)  
[PXE boot windows on Synology](https://www.synoforum.com/resources/how-to-pxe-boot-linux-windows-using-syslinux.115/)  

`copype amd64 C:\WinPE_amd64`  

`MakeWinPEMedia /ISO C:\WinPE_amd64 C:\WinPE_amd64\WinPE_amd64.iso`  

#### poss. 3
Get a windows 10 iso  

Extract everything into the windows folder  

Point to `sources/boot.wim`  

Put this iso on the pxe server  

#### Mounting for VMWare
Select the iso as disk and connect it to the virtual machine  

`lsblk`  

`cat /dev/(disk) > win10pe.iso`  

Copy iso to proper folder  
`rsync -v win10pe.iso /netboot/boot/amd64/windows/10/`  

## PXE config
`# mkdir /netboot/pxelinux.cfg`  

`# touch /netboot/pxelinux.cfg/default`  

Config file  
```
MENU TITLE  PXE Boot Menu
DEFAULT     vesamenu.c32

LABEL local
    MENU LABEL Boot from local drive
    LOCALBOOT 0xffff

MENU BEGIN amd64
MENU TITLE amd64

    MENU BEGIN Debian
    MENU TITLE Debian

        LABEL install
            MENU LABEL ^Install Debian 10
            KERNEL ::boot/amd64/debian/10-net/debian-installer/amd64/linux
            APPEND vga=788 initrd=::boot/amd64/debian/10-net/debian-installer/amd64/initrd.gz --- quiet

    MENU END

    MENU BEGIN Ubuntu
    MENU TITLE Ubuntu

        LABEL install
            MENU LABEL ^Install Ubuntu 20.04
            KERNEL ::boot/amd64/ubuntu/20-04/casper/vmlinuz
            APPEND initrd=::boot/amd64/ubuntu/20-04/casper/initrd nfsroot=192.168.7.10:/netboot/boot/amd64/ubuntu/20-04 netboot=nfs ip=dhcp --- quiet

    MENU END
    
    MENU BEGIN Windows
    MENU TITLE Windows
        
        LABEL install
            MENU LABEL ^Install Windows 10
            KERNEL memdisk
            INITRD ::boot/amd64/windows/10/sources/boot.wim
            
    MENU END

MENU END
```

`# ln -rs /netboot/pxelinux.cfg /netboot/bios && ln -rs /netboot/pxelinux.cfg /netboot/uefi`  

dnsmasq.conf  
```
dhcp-range=192.168.7.30,192.168.7.254,255.255.255.0,12h
dhcp-option=option:router,192.168.7.1
dhcp-option=option:dns-server,192.168.7.1

dhcp-boot=pxelinux.0,192.168.7.10,192.168.7.0

pxe-service=x86PC,"Network Boot (BIOS)",bios/pxelinux
pxe-service=X86-64_EFI,"Network Boot (EFI)",uefi/syslinux.efi

enable-tftp
tftp-root=/netboot

log-queries
log-facility=/var/log/dnsmasq.log
```

`# systemctl restart dnsmasq`  

Follow log  
`# tail -f /var/log/dnsmasq.log`  

`# chmod -R 777 /netboot`  

## NFS exports  
```
/netboot/boot/ *(ro,no_subtree_check,no_wdelay,sync,insecure_locks,no_root_squash,insecure)
```

`# systemctl restart nfs-kernel-server`  

## SMB
Edit `/etc/samba/smb.conf`  
```
[global]
        workgroup = WORKGROUP
        server string = Samba Server Version %v
        security = user
        passdb backend = tdbsam
        max log size = 50

[winos]
        comment = Install Windows Software
        path = /data/winos
        public = yes
        browseable = Yes
        writable = no
        printable = no
        valid users = debian
        guest ok = yes
        oplocks = no
        level2 oplocks = no
        locking = no
```

`# mkdir -p /data/winos/win10`  

`# smbpasswd -a debian`  

`# chmod -R 755 /data/winos`  

`# systemctl restart smbd`  

### Copy iso files
Add a windows 10 iso  

`# mount /dev/(disk) /mnt`  

`# cp -rv /mnt/* /data/winos/win10/`  

`# umount /mnt`  

## References
[Rasperry Pi as boot server](https://linuxconfig.org/how-to-configure-a-raspberry-pi-as-a-pxe-boot-server#h3-1-syslinux-files-and-modules)  
[Cofiguring pxe network boot server on Ubuntu 18.04](https://sxi.io/configuring-pxe-network-boot-server-on-ubuntu-18-04-lts/)  
[BitBastelei #277 - PXE: PCs ohne HDD über Netzwerk starten (Arch, ISOs, etc)](https://youtu.be/1XPwuMXJAHA)  
[Dnsmasq](https://wiki.archlinux.org/index.php/Dnsmasq)  
[PXE](https://wiki.archlinux.org/index.php/Preboot_Execution_Environment)  
