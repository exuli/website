# Arch Installation

Last Edited: 24.01.2021  

## Keyboard layout
`loadkeys de_CH-latin1`  

## Check UEFI mode
If the following command works, the system is booted in EFI.  
`ls /sys/firmware/efi/efivars`  

## Verify internet connection
`ping www.realstickman.net`  

## Update system clock
`timedatectl set-ntp true`  

## Creating partitions
`cfdisk`  

Use `EFI System` for EFI partition  
Use `Linux filesystem` for other partitions  

## (LUKS) Create encrypted partition
*Note: Do not put your /efi partition on an encrypted partition!*  

Create encrypted Partition  
`cryptsetup luksFormat /dev/(partition) --type luks1`  

To view if this worked correctly  
`cryptsetup luksDump /dev/(partition)`  

Open the partition  
Give it a fitting name  
`cryptsetup open /dev/(partition) (name)`  

Check if this worked with `ls /dev/mapper/`  
The name should show up there  

## Format partitions
Fat 32:  
`mkfs.fat -F32 /dev/(partition)`  
*For EFI or BOOT partition*  

Ext4:  
`mkfs.ext4 /dev/(partition)`  
*All other partitions*  

btrfs:  
`mkfs.btrfs /dev/(partition)`  
*All other partitions*  

## Mounting partitions
Generally partitions have to be mounted where you will later use them in your system.  
BTRFS with its subvolumes is a special case  
For EFI, prefer the /efi mountpoint  
```
Root: /mnt
EFI: /mnt/efi or /mnt/boot
Home: /mnt/home
etc...
```

### (BTRFS) Btrfs preparation of subvolumes and mounting
Mount root partition  
`mount /dev/(partition) /mnt`  

Root subvolume  
`btrfs subv create /mnt/@`  

Home subvolume  
`btrfs subv create /mnt/@home`  

Snapshots subvolume  
`btrfs subv create /mnt/@snapshots`  

Var subvolume  
`btrfs subv create /mnt/@var_log`  

*If you want to use a swapfile with Snapper, create a new subvolume now*  
Swap subvolume  
`btrfs subv create /mnt/@swap`  

`umount /mnt`  

Mount root  
`mount -o noatime,compress-force=zstd,space_cache=v2,subvol=@ /dev/(partition) /mnt`  

With /efi  
`mkdir -p /mnt/{efi,home,.snapshots,var/log,swap}`  
With /boot  
`mkdir -p /mnt/{boot,home,.snapshots,var/log,swap}`  

Mount home  
`mount -o noatime,compress-force=zstd,space_cache=v2,subvol=@home /dev/(partition) /mnt/home`

Mount snapshots  
`mount -o noatime,compress-force=zstd,space_cache=v2,subvol=@snapshots /dev/(partition) /mnt/.snapshots`

Mount var  
`mount -o noatime,compress-force=zstd,space_cache=v2,subvol=@var_log /dev/(partition) /mnt/var/log`

Swap subvolume  
`mount -o noatime,compress-force=zstd,space_cache=v2,subvol=@swap /dev/(partition) /mnt/swap`  

**Don't forget mounting other partitions!!**  

## Swap

### Swap partition
TODO

### Swapfile

#### Normal way
**NOT FOR BTRFS!**  
`dd if=/dev/zero of=/mnt/swapfile bs=1M count=(size) status=progress`  

#### (BTRFS) Swapfile in btrfs
*Does not work with snapper*  
*Use a separate subvolume in that case*  
`truncate -s 0 /mnt/swapfile`  

`chattr +C /mnt/swapfile`  

`btrfs property set /mnt/swapfile compression none`  

`fallocate -l (size)M /mnt/swapfile`  

#### Initialising swapfile
`chmod 600 /mnt/swapfile`  

`mkswap /mnt/swapfile`  

`swapon /mnt/swapfile`  

## Essential packages
Some things like the userspace utilities for file management will vary.  
See [file systems](https://wiki.archlinux.org/index.php/File_systems#Types_of_file_systems)  
`pacstrap /mnt base linux linux-firmware vim git openssh networkmanager network-manager-applet dialog base-devel linux-headers`  

### Microcode
`amd-ucode`  

`intel-ucode`  

### Filesystems
Fat32:  
`dosfstools mtools`  

Ext4:  
`e2fsprogs`  

Btrfs:  
`btrfs-progs compsize`  

### Wifi
`wpa_supplicant`  

### Snapper
`snapper`  

### Certificates
`ca-certificates ca-certificates-mozilla`  

### other
`cups hplip xdg-utils xdg-user-dirs inetutils`  

## Generate fstab
`genfstab -U /mnt >> /mnt/etc/fstab`  
**Make sure the fstab file has everything included**  

## Chroot into the system
`arch-chroot /mnt`  

## Set timezone
`ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime`  

## Set hardware clock
`hwclock --systohc`  

## Set locale
`vim /etc/locale.gen`  
Uncomment the locales that should be generated.  
Make sure to use a UTF-8 entry.  

`locale-gen`  

`echo "LANG=de_CH.UTF-8" > /etc/locale.conf`  

## Set keymap permanently
`echo "KEYMAP=de_CH-latin1" > /etc/vconsole.conf`  

## Set hostname
`echo "(hostname)" > /etc/hostname`  

Edit `/etc/hosts`  
```
127.0.0.1   localhost
::1         localhost
127.0.1.1   (hostname).localdomain  (hostname)
```

## Change root password
`passwd`  

## Bootloader installation

### GRUB UEFI
`pacman -S grub efibootmgr`  

If you are using encryption, see the next section first  
`grub-install --target=x86_64-efi --efi-directory=(efi partition mountpoint) --bootloader-id=GRUB`  

`grub-mkconfig -o /boot/grub/grub.cfg`  

#### (LUKS) Encryption
Create keyfile and add it to mkinitcpio.conf  
`dd bs=512 count=4 if=/dev/random of=/crypto_keyfile.bin iflag=fullblock`  

`chmod 600 /crypto_keyfile.bin`  

**This command should be run after installing a new kernel!**  
`chmod 600 /boot/initramfs-linux*`  

`cryptsetup luksAddKey /dev/(partition) /crypto_keyfile.bin`  

Include the file in `/etc/mkinitcpio.conf`  
`FILES=(/crypto_keyfile.bin)`  

Edit the `HOOKS` section as well. *Example:*  
`HOOKS=(base udev autodetect keyboard keymap modconf block encrypt filesystems fsck)`  

Edit `/etc/default/grub`  
`GRUB_ENABLE_CRYPTODISK=y`  

Some options in `GRUB_CMDLINE_LINUX_DEFAULT`  
**Make sure to change /dev/(partition) to UUID ASAP!**  
`cryptdevice=/dev/(partition):(name)`  
`cryptkey=/dev/mapper/(name):(filesystem):/crypto_keyfile.bin`  

## (BTRFS) Mkinitcpio
Load required modules  
`vim /etc/mkinitcpio.conf`  
`MODULES=(btrfs)`  
`BINARIES=("/usr/bin/btrfs")`  

`mkinitcpio -p linux`  

## Networking
`systemctl enable NetworkManager`  

## (CUPS) Printing
`systemctl enable cups`  

## Add user
`useradd -mG wheel (user)`  

Set password  
`passwd (user)`  

### Enable sudo
`visudo`  
Uncomment `%wheel ALL=(ALL) ALL`  

## Finishing installation
`exit`  
`poweroff`  
Remove the installation cd  

## (Snapper) Setup
`# umount /.snapshots`  

`# rm -r /.snapshots`  

Create snapper config  
`# snapper -c root create-config /`  

Delete unneeded volume  
`# btrfs subv del /.snapshots/`  

`# mkdir /.snapshots`  

Mount snapshots volume  
`# mount -a`  

`# chmod 750 /.snapshots`  

`# vim /etc/snapper/configs/root`  

Change these things:  
`ALLOW_USERS="(user)"`  
```
TIMELINE_LIMIT_HOURLY="5"
TIMELINE_LIMIT_DAILY="7"
TIMELINE_LIMIT_WEEKLY="0"
TIMELINE_LIMIT_MONTHLY="0"
TIMELINE_LIMIT_YEARLY="0"
```

Enable snapper  
`# systemctl enable --now snapper-timeline.timer`  
`# systemctl enable --now snapper-cleanup.timer`  

Allow user to access snapshots  
`# chmod a+rx /.snapshots`  
`# chown :(user) /.snapshots`  

## Install AUR helper

### yay
```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

### paru
```
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```

## (Snapper) Snap pac and GUI
`$ paru -S snap-pac-grub snapper-gui`  
