# Debian on Raspberry Pi 3

Last Edited: 25.12.2020  

## Image

The image can be downloaded from the following link.  
[https://raspi.debian.net/](https://raspi.debian.net/)  

By default the only user account available is root with an empty password.  

## Setup in tty

1. Create a [new user](/content/linux-other/users-groups.html)
    - Don't forget to add it to the "sudo" group
2. Check what IP address the device has
3. Install sudo

## Setup in ssh

1. Change the shell to bash with `chsh -s /bin/bash`
2. See [this](/content/linux-server/fixtermite.html) for instructions on fixing termite  
3. Change the hostname to something sensible
    - Change in /etc/hostname
    - Add the line `127.0.1.1   (hostname)` in /etc/hosts
